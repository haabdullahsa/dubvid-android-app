package com.ae.startup.dubvid.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.ae.startup.dubvid.R;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class HowToUseFragment extends Fragment {

    @InjectView(R.id.howToUseImage)
    ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_how_to_use, container, false);
        ButterKnife.inject(this,view);
        if (getArguments()!=null){
            image.setImageResource(getArguments().getInt("image",R.mipmap.logo));
        }
        return view;
    }


}
