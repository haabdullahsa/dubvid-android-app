package com.ae.startup.dubvid.fragment;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.ae.startup.dubvid.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class DubVideoFragment extends Fragment {


    @InjectView(R.id.dubVideoView)
    VideoView videoView;

    @InjectView(R.id.shareFacebook)
    ImageView facebook;

    @InjectView(R.id.shareWhatsapp)
    ImageView whatsapp;

    @InjectView(R.id.shareInstagram)
    ImageView instagram;



    @InjectView(R.id.messageShare)
    TextView messageShare;

    String videoId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_dub_video, container, false);
        ButterKnife.inject(this,view);
        MobileAds.initialize(getActivity().getApplicationContext(), "ca-app-pub-5878510798581582~5525463150");
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.loadAd(adRequest);

        if (getActivity().getIntent().getExtras()==null){
            getActivity().finish();
        }

        videoId=getActivity().getIntent().getExtras().getString("videoId",null);

        if (getArguments().getString("video")!=null){
            videoView.setVideoPath(getArguments().getString("video"));
            videoView.start();
        }else{
            getActivity().finish();
        }

        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                videoView.setVideoPath(getArguments().getString("video"));
                videoView.start();
                return true;
            }
        });





        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                whatsapp.animate().alpha(1).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                    instagram.animate().alpha(1).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            facebook.animate().alpha(1).setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    messageShare.animate().alpha(1);

                                    whatsapp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            createInstagramIntent("video/mp4",getArguments().getString("video"));
                                        }
                                    });

                                    instagram.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            createInstagramIntent("video/mp4",getArguments().getString("video"));
                                        }
                                    });

                                    facebook.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            createInstagramIntent("video/mp4",getArguments().getString("video"));
                                        }
                                    });
                                }


                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        });

        return view;
    }

    private void createInstagramIntent(String type, String mediaPath){

        // Create the new Intent using the 'Send' action.
        File file = new File(getArguments().getString("video"));

        Uri contentUri = FileProvider.getUriForFile(getActivity(), "com.ae.startup.dubvid", file);
        Intent share = new Intent(Intent.ACTION_SEND);

        // Set the MIME type
        share.setType(type);

        // Create the URI from the media
        File media = new File(mediaPath);
        Uri uri = Uri.fromFile(media);

        // Add the URI to the Intent.
        share.putExtra(Intent.EXTRA_STREAM, contentUri);


        // Broadcast the Intent.
        startActivity(Intent.createChooser(share, "Dubvid Uygulaması"));

        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference();
        final DatabaseReference ref= mDatabase.child("videos");
        ref.child("video"+videoId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Long x=(Long) dataSnapshot.child("downloadCount").getValue();
                ref.child("video"+videoId).child("downloadCount").setValue(x.intValue()+1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


}
