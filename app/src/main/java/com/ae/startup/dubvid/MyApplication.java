package com.ae.startup.dubvid;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by metin2 on 27.2.2017.
 */
public class MyApplication extends Application {

    FirebaseAuth firebaseAuth;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        firebaseAuth=FirebaseAuth.getInstance();
    }
}
