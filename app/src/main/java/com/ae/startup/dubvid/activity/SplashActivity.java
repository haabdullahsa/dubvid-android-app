package com.ae.startup.dubvid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ae.startup.dubvid.R;
import com.ae.startup.dubvid.util.SharedPref;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SplashActivity extends AppCompatActivity {

    @InjectView(R.id.splashLogo)
    ImageView logo;

    @InjectView(R.id.splashRootLayout)
    RelativeLayout splashRootLayout;

    int flipCount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        ButterKnife.inject(this);

        if(SharedPref.getInstance(this).hasFirstLaunch()){
            this.finish();
            startActivity(new Intent(SplashActivity.this,VideoListActivity.class));
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        }

        final Animation animationZoomIn= AnimationUtils.loadAnimation(this,R.anim.scale_max);
        final Animation animationZoomOut=AnimationUtils.loadAnimation(this,R.anim.scale_min);
        final Animation animationZoomFull=AnimationUtils.loadAnimation(this,R.anim.scale_full);
        logo.setAnimation(animationZoomFull);
        logo.setAnimation(animationZoomIn);
        logo.setAnimation(animationZoomOut);

        logo.startAnimation(animationZoomIn);

        animationZoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logo.startAnimation(animationZoomOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationZoomOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if(flipCount==3){
                    logo.setImageDrawable(getResources().getDrawable(R.mipmap.play_icon));
                    splashRootLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    logo.startAnimation(animationZoomFull);
                }else {
                    logo.startAnimation(animationZoomIn);
                    flipCount++;
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationZoomFull.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashRootLayout.setBackgroundColor(getResources().getColor(R.color.colorWhite));


                finish();
                startActivity(new Intent(SplashActivity.this,VideoListActivity.class));
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
