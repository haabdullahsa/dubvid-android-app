package com.ae.startup.dubvid.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ae.startup.dubvid.fragment.HowToUseFragment;
import com.ae.startup.dubvid.R;

/**
 * Created by metin2 on 22.2.2017.
 */
public class HowToUseViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final int NUM_PAGES = 3;
    public HowToUseViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle=new Bundle();
        HowToUseFragment howToUseDialogFragment=new HowToUseFragment();
        if (position==0){
            bundle.putInt("image", R.mipmap.info1);
        }else if (position==1){
            bundle.putInt("image",R.mipmap.info2);
        }else{
            bundle.putInt("image",R.mipmap.info3);
        }
        howToUseDialogFragment.setArguments(bundle);
        return howToUseDialogFragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
