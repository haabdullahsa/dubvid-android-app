package com.ae.startup.dubvid.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ae.startup.dubvid.R;
import com.ae.startup.dubvid.model.Video;
import com.ae.startup.dubvid.activity.VideoDetailActivity;

import java.util.ArrayList;

/**
 * Created by metin2 on 23.2.2017.
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {
     ArrayList<Video> videos;

    public VideoListAdapter(ArrayList<Video> videos){
        this.videos=videos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item= View.inflate(parent.getContext(), R.layout.layout_video_item,null);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.name.setText(videos.get(position).getName());
        holder.category.setText(holder.itemView.getContext().getResources().getString(R.string.text_type)+" "+videos.get(position).getCategoryId());
        holder.downloadCount.setText(videos.get(position).getDownloadCount()+" "+holder.itemView.getContext().getString(R.string.text_download_count));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(v.getContext(),VideoDetailActivity.class);

                i.putExtra("videoPath",videos.get(position).getVideoPath());
                i.putExtra("pathNoVoice",videos.get(position).getVideoNoVoicePath());
                i.putExtra("videoId",videos.get(position).getVideoId());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView category,downloadCount;

        public MyViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            category=(TextView)itemView.findViewById(R.id.category);
            downloadCount=(TextView)itemView.findViewById(R.id.download);


        }
    }
}
