package com.ae.startup.dubvid.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Abdullah on 23.2.2017.
 */


public class Video {

     private String categoryId;
    private String downloadCount;
    private String name;
    private String videoPath;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    private String videoId;

    public String getVideoNoVoicePath() {
        return videoNoVoicePath;
    }

    public void setVideoNoVoicePath(String videoNoVoicePath) {
        this.videoNoVoicePath = videoNoVoicePath;
    }

    private String videoNoVoicePath;

    public Video() {

    }

    public Video(String videoId,String categoryId, String name, String downloadCount, String videoPath,String videoNoVoicePath) {
        this.categoryId = categoryId;
        this.name = name;
        this.downloadCount = downloadCount;
        this.videoPath = videoPath;
        this.videoNoVoicePath=videoNoVoicePath;
        this.videoId = videoId;
    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(String downloadCount) {
        this.downloadCount = downloadCount;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }
}
