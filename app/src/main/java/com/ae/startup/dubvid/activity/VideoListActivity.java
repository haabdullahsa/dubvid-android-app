package com.ae.startup.dubvid.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ae.startup.dubvid.fragment.HowToUseDialogFragment;
import com.ae.startup.dubvid.R;
import com.ae.startup.dubvid.util.SharedPref;
import com.ae.startup.dubvid.model.Video;
import com.ae.startup.dubvid.adapter.VideoListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class VideoListActivity extends AppCompatActivity {

    @InjectView(R.id.listViewVideo)
    RecyclerView videoListView;

    VideoListAdapter videoListAdapter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        ButterKnife.inject(this);
        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference();


         progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Videolar hazırlanıyor");
        progressDialog.show();

        mDatabase.child("videos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 ArrayList<Video> videos=new ArrayList<>();
               for (DataSnapshot data:dataSnapshot.getChildren()){
                   Video video=new Video(String.valueOf(data.child("videoId").getValue()),String.valueOf(data.child("categoryId").getValue()),String.valueOf(data.child("name").getValue()),String.valueOf(data.child("downloadCount").getValue()),
                           String.valueOf(data.child("videoPath").getValue()),String.valueOf(data.child("videoNoVoicePath").getValue()));
                    videos.add(video);
               }
                videoListAdapter=new VideoListAdapter(videos);
                videoListView.setAdapter(videoListAdapter);
                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(VideoListActivity.this);
                videoListView.setLayoutManager(linearLayoutManager);
                videoListView.setHasFixedSize(true);
                progressDialog.dismiss();
                if (!SharedPref.getInstance(VideoListActivity.this).hasFirstLaunch()){
                    HowToUseDialogFragment howToUseDialogFragment=new HowToUseDialogFragment();
                    howToUseDialogFragment.show(getSupportFragmentManager(),"");
                    SharedPref.getInstance(VideoListActivity.this).setFirstLaunch();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(VideoListActivity.this, getResources().getString(R.string.text_internet_connection),Toast.LENGTH_LONG).show();
            }
        });






    }
}
