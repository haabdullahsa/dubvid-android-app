package com.ae.startup.dubvid.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ae.startup.dubvid.R;
import com.ae.startup.dubvid.fragment.VideoDetailFragment;

public class VideoDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);

        if (getIntent().getExtras() == null){
            finish();
        }
        String path=getIntent().getExtras().getString("videoPath",null);
        String pathNoVoice=getIntent().getExtras().getString("pathNoVoice",null);
        String videoId = getIntent().getExtras().getString("videoId",null);

        VideoDetailFragment videoDetailFragment=new VideoDetailFragment();
        Bundle bundle=new Bundle();
        bundle.putString("path",path);
        bundle.putString("pathNoVoice",pathNoVoice);
        bundle.putString("videoId",videoId);

        videoDetailFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment,videoDetailFragment).commit();



    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
