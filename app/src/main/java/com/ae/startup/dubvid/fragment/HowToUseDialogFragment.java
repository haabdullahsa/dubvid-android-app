package com.ae.startup.dubvid.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.ae.startup.dubvid.R;
import com.ae.startup.dubvid.adapter.HowToUseViewPagerAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class HowToUseDialogFragment extends DialogFragment {

        @InjectView(R.id.bttnCancelDialog)
        Button cancel;

        @InjectView(R.id.howToUseViewPager)
            ViewPager howToUsePager;

    @InjectView(R.id.indicator)
    CirclePageIndicator circlePageIndicator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
       View root= inflater.inflate(R.layout.fragment_how_to_use_dialog, container, false);
        ButterKnife.inject(this,root);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        howToUsePager.setAdapter(new HowToUseViewPagerAdapter(getChildFragmentManager()));
        circlePageIndicator.setViewPager(howToUsePager);
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setWindowAnimations(
                R.style.dialog_slide_animation);

        int dialogWidth = 650; // specify a value here
        int dialogHeight = 750; // specify a value here

        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
    }
}
