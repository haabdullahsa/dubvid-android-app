package com.ae.startup.dubvid.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.ae.startup.dubvid.R;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class VideoDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    @InjectView(R.id.spin_kit)
    SpinKitView spinKitView;

    @InjectView(R.id.videoView)
    VideoView videoView;

    @InjectView(R.id.microphoneIV)
    ImageView microphone;

    ProgressDialog progressDialog;

    String AudioSavePathInDevice = null;

    MediaRecorder mediaRecorder ;

    ProgressDialog progressMerge;
     File localFile,noVoiceFile;

    File dubFile;

    FFmpeg ffmpeg;

    StorageReference pathReference;
    Uri uriVideo;

    boolean isSuccessNoVoiceVideo=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_video_detail, container, false);
        String path="";
        String pathNoVoice="";
        String videoId="";
        ButterKnife.inject(this,view);

        if (getArguments().getString("path")!=null){
            path=getArguments().getString("path");
            pathNoVoice=getArguments().getString("pathNoVoice");
            videoId=getArguments().getString("videoId");
        }



        progressMerge=new ProgressDialog(getActivity());

        ffmpeg = FFmpeg.getInstance(getActivity());
        loadFFMpegBinary();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

         pathReference = storageRef.child(path);
        StorageReference pathNoVoiceReference = storageRef.child(pathNoVoice);

        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.text_progress_open_video));
        progressDialog.setCancelable(false);
        progressDialog.show();


        try {
            localFile = File.createTempFile("video_dubvid",".mp4");
            noVoiceFile= File.createTempFile("video_dubvid_no_voice",".mp4");

            pathReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    uriVideo=uri;
                    videoView.setVideoURI(uri);
                    videoView.requestFocus();
                }
            });



        pathNoVoiceReference.getFile(Uri.fromFile(noVoiceFile)).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                progressDialog.dismiss();
                videoView.start();


                microphone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(getActivity())
                                .setIcon(R.mipmap.microphone_info)
                                .setTitle(getResources().getString(R.string.text_ready))
                                .setMessage(getResources().getString(R.string.text_ıwannastart))
                                .setCancelable(false)
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        Toast.makeText(getActivity(), getResources().getString(R.string.text_on_air),Toast.LENGTH_LONG).show();
                                        videoView.setVideoPath(noVoiceFile.getPath());
                                        videoView.requestFocus();
                                        videoView.start();
                                        AudioSavePathInDevice =
                                                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                                        "AudioRecording.3gp";

                                        MediaRecorderReady();

                                        try {
                                            mediaRecorder.prepare();
                                            mediaRecorder.start();
                                            videoView.start();

                                        } catch (IllegalStateException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                        microphone.setVisibility(View.GONE);
                                        spinKitView.setVisibility(View.VISIBLE);



                                    }
                                })
                                .create()
                                .show();

                    }
                });
            }


        });


        } catch (IOException e) {
            e.printStackTrace();
        }





        videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!videoView.isPlaying()){
                    videoView.setVideoPath(localFile.getPath());
                    videoView.requestFocus();
                    videoView.start();
                }
                return true;
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (spinKitView.getVisibility()==View.VISIBLE){
                    mediaRecorder.stop();
                    spinKitView.setVisibility(View.GONE);
                    microphone.setVisibility(View.VISIBLE);



                    try {


                        dubFile=File.createTempFile("video_dubvid_dub",".mp4");

                        String cmd = "-i "+noVoiceFile.getPath()+" -i "+AudioSavePathInDevice+" -map 0:v -map 1:a -c copy -y "+dubFile.getPath();
                        String[] command = cmd.split(" ");
                        ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {

                            @Override
                            public void onStart() {
                                Log.d("ffmpeg", "Started command : ffmpeg");
                                progressMerge.setMessage(getResources().getString(R.string.text_creating_video));
                                progressMerge.show();
                            }

                            @Override
                            public void onProgress(String message) {
                                Log.d("ffmpeg", "progress: "+message);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.d("ffmpeg", "failed: "+message);
                            }

                            @Override
                            public void onSuccess(String message) {
                                Log.d("ffmpeg", "success: "+message);
                                DubVideoFragment dubVideoFragment=new DubVideoFragment();
                                Bundle bundle=new Bundle();
                                bundle.putString("video",dubFile.getPath());
                                dubVideoFragment.setArguments(bundle);
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment,dubVideoFragment).commit();
                            }

                            @Override
                            public void onFinish() {
                                progressMerge.dismiss();
                                Toast.makeText(getActivity(), getResources().getString(R.string.text_your_video_is_ready),Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        // Handle if FFmpeg is already running
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });


        return view;

    }

    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void loadFFMpegBinary() {
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }


    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getResources().getString(R.string.text_fail))
                .setMessage(getResources().getString(R.string.text_failed_your_device))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .create()
                .show();

    }








}
