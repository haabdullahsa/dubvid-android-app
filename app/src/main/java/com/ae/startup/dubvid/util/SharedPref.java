package com.ae.startup.dubvid.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by metin2 on 25.2.2017.
 */
public class SharedPref {

    private static SharedPref instance=null;
   private static SharedPreferences sharedPreferences;

    public static SharedPref getInstance(Activity activity){
        if (instance==null){
            instance=new SharedPref();
            sharedPreferences=activity.getSharedPreferences("dubvidfile", Context.MODE_PRIVATE);
        }
        return instance;
    }

    public void setFirstLaunch(){
        sharedPreferences.edit().putBoolean("launch",true).commit();
    }

    public boolean hasFirstLaunch(){
        return sharedPreferences.getBoolean("launch",false);
    }


}
