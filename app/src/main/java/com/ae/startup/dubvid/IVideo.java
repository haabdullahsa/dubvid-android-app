package com.ae.startup.dubvid;

import com.ae.startup.dubvid.model.Video;

import java.util.ArrayList;

/**
 * Created by Abdullah on 23.2.2017.
 */
public interface IVideo {
    public ArrayList<Video> videoList();
}
